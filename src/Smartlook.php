<?php

namespace Drupal\smartlook_tracking;

use Drupal\Core\Cache\Cache;

/**
 * A class for the Smartlook Tracking module.
 *
 * @package Drupal\smartlook_tracking
 */
class Smartlook {

  /**
   * Insert JavaScript to the appropriate scope/region of the page.
   *
   * @param array $attachments
   *   An array that you can add attachments to.
   */
  public static function hookPageAttachments(array &$attachments) {
    $user = \Drupal::currentUser();
    $config = \Drupal::config('smartlook_tracking.settings');

    $key = $config->get('account');
    $region = $config->get('region') ?? 'eu';

    $visibility_pages = _smartlook_tracking_visibility_pages();
    $visibility_user = _smartlook_tracking_visibility_user($user);

    if (!empty($key) && $visibility_pages && $visibility_user) {
      // Build tracker code.
      $script = "window.smartlook||(function(d) {";
      $script .= "var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];";
      $script .= "var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';";
      $script .= "c.charset='utf-8';c.src='https://web-sdk.smartlook.com/recorder.js';h.appendChild(c);";
      $script .= "})(document);";
      $script .= "smartlook('init', '" . $key . "', { region: '" . $region . "' });";

      $attachments['#attached']['html_head'][] = [
        [
          '#tag'   => 'script',
          '#value' => $script,
        ],
        'smartlook_tracking_script',
      ];
    }

    $attachments['#cache']['tags'] = Cache::mergeTags($attachments['#cache']['tags'] ?? [], $config->getCacheTags());
  }

}
