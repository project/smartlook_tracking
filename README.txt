Module: Smartlook tracking
Author: Sándor Juhász (lonalore) <https://www.drupal.org/u/lonalore>


Description
===========

Adds the Smartlook tracking system to your website.


Requirements
============

- [Smartlook](https://www.smartlook.com) account


Installation
============
- Copy the 'smartlook_tracking' module directory in to your Drupal 'modules'
  directory as usual. Or install it via Composer
- Enable the module
- In the settings page enter your Smartlook ID
- Give permissions to "smartlook tracking" to any role you want to track.
  This is usually anonymous users
